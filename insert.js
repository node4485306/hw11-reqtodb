const { useBase } = require('./base');
const { options } = require('./cli-options')

const insert = async () => {
    try {
        const { base } = await useBase();

        await base('newsPosts').insert({
            title: options.title,
            text: options.text,
            created_at: new Date()
        })
        console.log("Data inserted")

    } catch (error) {
        console.log(error)
    }
}

(async () => {
    await insert();
})()

