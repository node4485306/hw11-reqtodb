const knex = require('knex')

const useBase = async () => {
    const base = knex({
        client: 'mysql2',
        debug: true,
        connection: {
            host: '127.0.0.1',
            user: 'root',
            port: 3306,
            password: '',
        }
    });

    const init = async () => {
        await base.raw('CREATE DATABASE IF NOT EXISTS news');
        await base.raw('USE news');
    }

    await init()

    return {
        base
    }
}


module.exports = {
  useBase
}


