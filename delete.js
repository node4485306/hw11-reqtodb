const { useBase } = require('./base');
const { options } = require('./cli-options')

const deletePost = async () => {
    try {
        const { base } = await useBase();

        if(!options.id) {
            throw new Error("Please input id")
        }

        const newsPost = await base('newsPosts').where({ id: options.id }).delete();
        console.log("Data deleted");
        return newsPost;

    } catch (error) {
        console.log(error)
    }
}

(async () => {
    await deletePost();
})()

