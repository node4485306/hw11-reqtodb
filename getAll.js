const { useBase } = require('./base');
const { options } = require('./cli-options')

const getAll = async () => {
    try {
        const { base } = await useBase();

        const allNewsPosts = await base('newsPosts').select('*');
        return allNewsPosts;

    } catch (error) {
        console.log(error)
    }
}

(async () => {
    console.table(await getAll())
})()

