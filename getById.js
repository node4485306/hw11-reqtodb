const { useBase } = require('./base');
const { options } = require('./cli-options')

const getById = async () => {
    try {
        const { base } = await useBase();

        if(!options.id) {
            throw new Error("Please input id")
        }

        const newsPost = await base('newsPosts').where({ id: options.id }).select('*');
        return newsPost;

    } catch (error) {
        console.log(error)
    }
}

(async () => {
    console.table(await getById())
})()

