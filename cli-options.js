const commandLineArgs = require('command-line-args');

const optionsDefinition = [ 
    { name: 'id', type: Number },
    { name: 'title', type: String },
    { name: 'text', type: String }
]

const options = commandLineArgs(optionsDefinition);

module.exports = { options }