const { useBase } = require('./base');
const { options } = require('./cli-options')

const update = async () => {
    try {
        const { base } = await useBase();

        if(!options.id) {
            throw new Error("Please input id")
        }

        const data = {};

        for (let key in options) {
            if (key !== 'id') {
                data[key] = options[key];
            }
        }

        data['updated_at'] = new Date();

        await base('newsPosts').where({ id: options.id }).update({
            title: options.title,
            text: options.text,
            updated_at: new Date()
        })

        console.log("Data updated")

    } catch (error) {
        console.log(error)
    }
}

(async () => {
    await update();
})()

